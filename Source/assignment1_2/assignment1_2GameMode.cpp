// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "assignment1_2GameMode.h"
#include "assignment1_2HUD.h"
#include "assignment1_2Character.h"
#include "UObject/ConstructorHelpers.h"

Aassignment1_2GameMode::Aassignment1_2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aassignment1_2HUD::StaticClass();
}
