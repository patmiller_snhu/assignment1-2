// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "assignment1_2HUD.generated.h"

UCLASS()
class Aassignment1_2HUD : public AHUD
{
	GENERATED_BODY()

public:
	Aassignment1_2HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

