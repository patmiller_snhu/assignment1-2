// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT1_2_assignment1_2Character_generated_h
#error "assignment1_2Character.generated.h already included, missing '#pragma once' in assignment1_2Character.h"
#endif
#define ASSIGNMENT1_2_assignment1_2Character_generated_h

#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_RPC_WRAPPERS
#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAassignment1_2Character(); \
	friend struct Z_Construct_UClass_Aassignment1_2Character_Statics; \
public: \
	DECLARE_CLASS(Aassignment1_2Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/assignment1_2"), NO_API) \
	DECLARE_SERIALIZER(Aassignment1_2Character)


#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAassignment1_2Character(); \
	friend struct Z_Construct_UClass_Aassignment1_2Character_Statics; \
public: \
	DECLARE_CLASS(Aassignment1_2Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/assignment1_2"), NO_API) \
	DECLARE_SERIALIZER(Aassignment1_2Character)


#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aassignment1_2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aassignment1_2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aassignment1_2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aassignment1_2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aassignment1_2Character(Aassignment1_2Character&&); \
	NO_API Aassignment1_2Character(const Aassignment1_2Character&); \
public:


#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aassignment1_2Character(Aassignment1_2Character&&); \
	NO_API Aassignment1_2Character(const Aassignment1_2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aassignment1_2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aassignment1_2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aassignment1_2Character)


#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(Aassignment1_2Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(Aassignment1_2Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(Aassignment1_2Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(Aassignment1_2Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(Aassignment1_2Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(Aassignment1_2Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(Aassignment1_2Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(Aassignment1_2Character, L_MotionController); }


#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_11_PROLOG
#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_PRIVATE_PROPERTY_OFFSET \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_RPC_WRAPPERS \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_INCLASS \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_PRIVATE_PROPERTY_OFFSET \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_INCLASS_NO_PURE_DECLS \
	assignment1_2_Source_assignment1_2_assignment1_2Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT1_2_API UClass* StaticClass<class Aassignment1_2Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID assignment1_2_Source_assignment1_2_assignment1_2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
