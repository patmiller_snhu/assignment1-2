// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "assignment1_2/assignment1_2HUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeassignment1_2HUD() {}
// Cross Module References
	ASSIGNMENT1_2_API UClass* Z_Construct_UClass_Aassignment1_2HUD_NoRegister();
	ASSIGNMENT1_2_API UClass* Z_Construct_UClass_Aassignment1_2HUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_assignment1_2();
// End Cross Module References
	void Aassignment1_2HUD::StaticRegisterNativesAassignment1_2HUD()
	{
	}
	UClass* Z_Construct_UClass_Aassignment1_2HUD_NoRegister()
	{
		return Aassignment1_2HUD::StaticClass();
	}
	struct Z_Construct_UClass_Aassignment1_2HUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aassignment1_2HUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_assignment1_2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aassignment1_2HUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "assignment1_2HUD.h" },
		{ "ModuleRelativePath", "assignment1_2HUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aassignment1_2HUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aassignment1_2HUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aassignment1_2HUD_Statics::ClassParams = {
		&Aassignment1_2HUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Aassignment1_2HUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_Aassignment1_2HUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aassignment1_2HUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aassignment1_2HUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aassignment1_2HUD, 524136062);
	template<> ASSIGNMENT1_2_API UClass* StaticClass<Aassignment1_2HUD>()
	{
		return Aassignment1_2HUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aassignment1_2HUD(Z_Construct_UClass_Aassignment1_2HUD, &Aassignment1_2HUD::StaticClass, TEXT("/Script/assignment1_2"), TEXT("Aassignment1_2HUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aassignment1_2HUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
