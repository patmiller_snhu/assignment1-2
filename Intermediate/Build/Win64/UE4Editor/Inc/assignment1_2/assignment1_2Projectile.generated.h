// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef ASSIGNMENT1_2_assignment1_2Projectile_generated_h
#error "assignment1_2Projectile.generated.h already included, missing '#pragma once' in assignment1_2Projectile.h"
#endif
#define ASSIGNMENT1_2_assignment1_2Projectile_generated_h

#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAassignment1_2Projectile(); \
	friend struct Z_Construct_UClass_Aassignment1_2Projectile_Statics; \
public: \
	DECLARE_CLASS(Aassignment1_2Projectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/assignment1_2"), NO_API) \
	DECLARE_SERIALIZER(Aassignment1_2Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAassignment1_2Projectile(); \
	friend struct Z_Construct_UClass_Aassignment1_2Projectile_Statics; \
public: \
	DECLARE_CLASS(Aassignment1_2Projectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/assignment1_2"), NO_API) \
	DECLARE_SERIALIZER(Aassignment1_2Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Aassignment1_2Projectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aassignment1_2Projectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aassignment1_2Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aassignment1_2Projectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aassignment1_2Projectile(Aassignment1_2Projectile&&); \
	NO_API Aassignment1_2Projectile(const Aassignment1_2Projectile&); \
public:


#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Aassignment1_2Projectile(Aassignment1_2Projectile&&); \
	NO_API Aassignment1_2Projectile(const Aassignment1_2Projectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Aassignment1_2Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aassignment1_2Projectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aassignment1_2Projectile)


#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(Aassignment1_2Projectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(Aassignment1_2Projectile, ProjectileMovement); }


#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_9_PROLOG
#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_RPC_WRAPPERS \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_INCLASS \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_INCLASS_NO_PURE_DECLS \
	assignment1_2_Source_assignment1_2_assignment1_2Projectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT1_2_API UClass* StaticClass<class Aassignment1_2Projectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID assignment1_2_Source_assignment1_2_assignment1_2Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
