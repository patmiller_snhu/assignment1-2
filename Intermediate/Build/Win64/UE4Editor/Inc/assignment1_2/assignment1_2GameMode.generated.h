// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASSIGNMENT1_2_assignment1_2GameMode_generated_h
#error "assignment1_2GameMode.generated.h already included, missing '#pragma once' in assignment1_2GameMode.h"
#endif
#define ASSIGNMENT1_2_assignment1_2GameMode_generated_h

#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_RPC_WRAPPERS
#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAassignment1_2GameMode(); \
	friend struct Z_Construct_UClass_Aassignment1_2GameMode_Statics; \
public: \
	DECLARE_CLASS(Aassignment1_2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/assignment1_2"), ASSIGNMENT1_2_API) \
	DECLARE_SERIALIZER(Aassignment1_2GameMode)


#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAassignment1_2GameMode(); \
	friend struct Z_Construct_UClass_Aassignment1_2GameMode_Statics; \
public: \
	DECLARE_CLASS(Aassignment1_2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/assignment1_2"), ASSIGNMENT1_2_API) \
	DECLARE_SERIALIZER(Aassignment1_2GameMode)


#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ASSIGNMENT1_2_API Aassignment1_2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Aassignment1_2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASSIGNMENT1_2_API, Aassignment1_2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aassignment1_2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ASSIGNMENT1_2_API Aassignment1_2GameMode(Aassignment1_2GameMode&&); \
	ASSIGNMENT1_2_API Aassignment1_2GameMode(const Aassignment1_2GameMode&); \
public:


#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ASSIGNMENT1_2_API Aassignment1_2GameMode(Aassignment1_2GameMode&&); \
	ASSIGNMENT1_2_API Aassignment1_2GameMode(const Aassignment1_2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ASSIGNMENT1_2_API, Aassignment1_2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Aassignment1_2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Aassignment1_2GameMode)


#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_9_PROLOG
#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_RPC_WRAPPERS \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_INCLASS \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	assignment1_2_Source_assignment1_2_assignment1_2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASSIGNMENT1_2_API UClass* StaticClass<class Aassignment1_2GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID assignment1_2_Source_assignment1_2_assignment1_2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
