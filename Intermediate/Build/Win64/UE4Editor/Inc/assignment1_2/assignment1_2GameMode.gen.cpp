// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "assignment1_2/assignment1_2GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeassignment1_2GameMode() {}
// Cross Module References
	ASSIGNMENT1_2_API UClass* Z_Construct_UClass_Aassignment1_2GameMode_NoRegister();
	ASSIGNMENT1_2_API UClass* Z_Construct_UClass_Aassignment1_2GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_assignment1_2();
// End Cross Module References
	void Aassignment1_2GameMode::StaticRegisterNativesAassignment1_2GameMode()
	{
	}
	UClass* Z_Construct_UClass_Aassignment1_2GameMode_NoRegister()
	{
		return Aassignment1_2GameMode::StaticClass();
	}
	struct Z_Construct_UClass_Aassignment1_2GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Aassignment1_2GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_assignment1_2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Aassignment1_2GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "assignment1_2GameMode.h" },
		{ "ModuleRelativePath", "assignment1_2GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Aassignment1_2GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Aassignment1_2GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Aassignment1_2GameMode_Statics::ClassParams = {
		&Aassignment1_2GameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802A8u,
		METADATA_PARAMS(Z_Construct_UClass_Aassignment1_2GameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_Aassignment1_2GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Aassignment1_2GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Aassignment1_2GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Aassignment1_2GameMode, 1515441228);
	template<> ASSIGNMENT1_2_API UClass* StaticClass<Aassignment1_2GameMode>()
	{
		return Aassignment1_2GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Aassignment1_2GameMode(Z_Construct_UClass_Aassignment1_2GameMode, &Aassignment1_2GameMode::StaticClass, TEXT("/Script/assignment1_2"), TEXT("Aassignment1_2GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Aassignment1_2GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
